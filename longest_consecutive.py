def longest_consecutive(nums):
    if len(nums) == 0:
        return 0
    num_set = set(nums)
    max_length = 0
    for num in num_set:
        if num - 1 not in num_set:
            current_num = num
            current_len = 1
            while current_num + 1 in num_set:
                current_num +=1
                current_len += 1
            max_length = max(max_length, current_len)
    return max_length
            
# Test cases
nums1 = [100, 4, 200, 1, 3, 2]
nums2 = [0, 3, 7, 2, 5, 8, 4, 6, 0, 1]
print(longest_consecutive(nums1))  # Output: 4
print(longest_consecutive(nums2))  # Output: 9
