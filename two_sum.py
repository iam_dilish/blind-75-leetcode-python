def two_sum(a, num):
    size = len(a)
    for i in range(0, size):
        for j in range(i + 1, size):
            if a[i] + a[j] == num:
                return [i, j]
            
nums1, target1 = [2, 7, 11, 15], 9
nums2, target2 = [3, 2, 4], 6
nums3, target3 = [3, 3], 6

print(two_sum(nums1, target1))  # Output: [0, 1]
print(two_sum(nums2, target2))  # Output: [1, 2]
print(two_sum(nums3, target3))  # Output: [0, 1]
